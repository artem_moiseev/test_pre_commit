## Linters configuration and use
The project uses tools to keep the code clean, such as:
* ruff
* mypy
* pre-commit

Linters/formatters are configured in **pyproject.toml**.

Pre-commit is configured in **.pre-commit-config.yaml**.

To enable the usage of linters before commits/pushes, you need to install python **pre-commit** library
```bash
$ pip install pre-commit
```
or use make command, defined in **Makefile**
```bash
$ make refresh_pre_commit
```

To run pre-commit checks manually, run
```bash
$ pre-commit
or
$ python -m pre_commit
```

To run ruff and mypy checks:
```bash
$ ruff check .
$ mypy .
```
