"""Test module."""


def multiply_numbers(
    n1: float,
    n2: float,
) -> float:
    """Multiply 2 numbers.

    Args:
    ----
        n1 (float): argument 1
        n2 (float): argument 2

    Returns:
    -------
        float: result of n1 * n2

    """
    return n1 * n2


def add_numbers(
    n1: float,
    n2: float,
) -> float:
    """Add 2 numbers.

    Args:
    ----
        n1 (float): argument 1
        n2 (float): argument 2

    Returns:
    -------
        float: result of n1 + n2

    """
    return n1 + n2


if __name__ == "__main__":
    print(multiply_numbers(n1=10, n2=20))
    print(add_numbers(n1=10, n2=20))
