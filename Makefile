.PHONY: run clean_env clean_all refresh_env refresh_pre_commit
VENV = env
PYTHON = $(VENV)/bin/python
PIP = $(VENV)/bin/pip
AUTOUPDATE_PRE_COMMIT = TRUE

run: $(VENV)/bin/activate
	$(PYTHON) main.py

refresh_env: $(VENV)/bin/activate

$(VENV)/bin/activate: requirements.txt
	python -m venv $(VENV)
	$(PIP) install --upgrade pip
	$(PIP) install -r requirements.txt

clean_env:
	rm -rf __pycache__
	rm -rf $(VENV)

refresh_pre_commit: .pre-commit-config.yaml $(VENV)/bin/activate
	$(PIP) install pre-commit
	$(PYTHON) -m pre_commit install
ifeq ($(AUTOUPDATE_PRE_COMMIT),TRUE)
	$(PYTHON) -m pre_commit autoupdate
endif

clean_all: clean_env
	rm -rf .mypy_cache
	rm -rf .ruff_cache
