# MLOps course repository

To configure environment and run a program, use this command:
```zsh
$ make run
```

To remove environment and cache files, run:
```zsh
$ make clean_all
```

To remove only environment, run:
```zsh
$ make clean_env
```

To refresh environment from requirements.txt, run:
```zsh
$ make refresh_env
```

To update pre-commit from settings, run:
```zsh
$ make refresh_pre_commit
```
